"""Classes and functions to power newssearch functionality."""

import argparse
import sys


def parse_args(args=None):

    cli_parser = argparse.ArgumentParser()

    cli_parser.add_argument(
        "-b",
        "--boolean_operation",
        default="OR",
        help="Whether to combine search terms using boolean AND or OR.",
        choices=["OR", "AND"],
        type=str.upper,
    )
    cli_news_arg = cli_parser.add_mutually_exclusive_group(required=True)
    cli_news_arg.add_argument(
        "-f", "--file", help="Path to file containing news to search", default=None
    )
    cli_news_arg.add_argument(
        "-u", "--url", help="Url which will be used to get news to search", default=None
    )

    cli_parser.add_argument("search_terms", help="terms to search for", nargs="+")

    return cli_parser.parse_args(args or sys.argv[1:])


class Grep:
    """Class to power grep like searches of know text."""

    def __init__(self, lines=None):
        """Initialise with lines of text to search."""

        self.lines = lines or []

    def search(self, boolean_operation, terms):
        """Use boolean_operation to combine terms and search self.lines.

        :param boolean_operation: either AND or OR
        :param terms: list of strings to search for.

        :returns: list of indexes of matching lines.

        """

        # default to disjunction search (OR)
        search_func = self.disjunction_search

        if boolean_operation.upper() == "AND":
            search_func = self.conjunction_search

        return list(set(search_func(terms)))

    def disjunction_search(self, terms):
        """Combine terms with boolean OR and return results.

        :param terms: list of strings to search for.

        :returns: list of tuples of indexes of matching lines, and line.

        >>> lines = ['zero', 'one', 'two', 'three']
        >>> grepper = Grep(lines)
        >>> grepper.disjunction_search(['on', 'r'])
        [
            (0, 'zero'),
            (1, 'one'),
            (3, 'three'),
        ]

        """

        rv = []

        for index, line in enumerate(self.lines):
            for term in terms:
                if term in line:
                    rv.append((index, line))

        return rv

    def conjunction_search(self, terms):
        """Combine terms with boolean AND and return results.

        :param terms: list of strings to search for.

        :returns: list of tuples of indexes of matching lines, and line.

        >>> lines = ['zero', 'one', 'two', 'three']
        >>> grepper = Grep(lines)
        >>> grepper.disjunction_search(['o', 'r'])
        [
            (0, 'zero'),
        ]

        """

        rv = []

        for index, line in enumerate(self.lines):
            for term in terms:
                if term not in line:
                    break
            else:
                rv.append((index, line))

        return rv
