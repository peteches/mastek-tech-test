"""import generally used things."""

import sys

import requests

from .newssearch import Grep, parse_args


def main(args=None):
    """Entrypoint for newssearch."""

    parsed_args = parse_args(args)

    text = []

    if parsed_args.file:
        with open(parsed_args.file, "r") as NEWS:
            text = NEWS.readlines()
    elif parsed_args.url:
        resp = requests.get(parsed_args.url)
        resp.raise_for_status()

        text = resp.text.split("\n")
    else:
        raise ArgumentError("No File source found")

    searcher = Grep(text)

    return sorted(
        searcher.search(
            boolean_operation=parsed_args.boolean_operation,
            terms=parsed_args.search_terms,
        )
    )
