"""Nose tests"""

import os
import sys
import tempfile
from unittest import mock

import requests
from nose2.tools import such

import newssearch

with such.A("newssearch package") as it:

    @it.has_setup
    def test_setup():
        it.stderr_old, it.stdout_old = sys.stderr, sys.stdout
        # silence some errors when testing cli opts.
        dn = open(os.devnull, 'w')
        sys.stderr, sys.stdout = dn, dn
        it.news_url = "https://gist.githubusercontent.com/edhiley/fdf7793d3d2c9e838c11/raw/ef09a74e925e051c6a756ec54b6adba1787d937b/hscic-news"

        response = requests.get(it.news_url)
        it.url_lines = response.text.split("\n")

        it.news_file = tempfile.mkstemp()[1]

        with open(it.news_file, "w") as fd:
            fd.writelines(it.url_lines)

    @it.has_teardown
    def teardown():
        os.remove(it.news_file)

        sys.stderr, sys.stdout = it.stderr_old, it.stdout_old

    with it.having("a main() entry point"):

        @it.should("source news from a local file")
        def ns_file_main():
            with mock.patch("newssearch.Grep") as g_mock:
                grep_mock = mock.MagicMock()
                g_mock.return_value = grep_mock

                args = ["--file", it.news_file, "search", "term"]
                newssearch.main(args)

                lines = []
                with open(it.news_file, "r") as NEWS:
                    lines = NEWS.readlines()

                g_mock.assert_called_with(lines)
                grep_mock.search.assert_called_with(
                    boolean_operation="OR", terms=["search", "term"]
                )

        @it.should("source news from a url")
        def ns_url_main():
            with mock.patch("newssearch.Grep") as g_mock:
                grep_mock = mock.MagicMock()
                g_mock.return_value = grep_mock

                args = ["--url", it.news_url, "search"]
                newssearch.main(args)

                g_mock.assert_called_with(it.url_lines)
                grep_mock.search.assert_called_with(
                    boolean_operation="OR", terms=["search"]
                )

    with it.having("a function to parse arguments"):

        @it.should("raise an exception if neither file nor url provided")
        def ns_no_source_cli():
            with it.assertRaises(SystemExit):
                newssearch.parse_args(["search"])

        @it.should("raise an exception if no search terms provided")
        def ns_no_source_cli():
            with it.assertRaises(SystemExit):
                newssearch.parse_args(["--url", "https://www.example.com"])

        @it.should("allow use of either 'AND' or 'OR' types.")
        def ns_type_cli():
            defaults = ["-u", "https://example.com", "search"]

            args = newssearch.parse_args(defaults)
            it.assertEqual(args.boolean_operation, "OR")

            for boolean_operation in ["or", "and", "OR", "AND"]:
                args = newssearch.parse_args(
                    ["--boolean_operation", boolean_operation] + defaults
                )
                it.assertEqual(args.boolean_operation, boolean_operation.upper())

    with it.having("a Grep class"):

        @it.should("instantiate with an optional list of strings")
        def ns_init_grep():
            g = newssearch.Grep()
            it.assertEqual(g.lines, [])

            g = newssearch.Grep(["one", "two"])
            it.assertEqual(g.lines, ["one", "two"])

        @it.should("have a seach method that proxies search to the appropriate method")
        def ns_search_grep():
            with mock.patch.object(newssearch.Grep, "disjunction_search") as grep_mock:
                terms = ["one", "two"]
                g = newssearch.Grep()

                g.search("OR", terms)

                grep_mock.assert_called_with(terms)

            with mock.patch.object(newssearch.Grep, "conjunction_search") as grep_mock:
                terms = ["one", "two"]
                g = newssearch.Grep()

                g.search("AND", terms)

                grep_mock.assert_called_with(terms)

        @it.should("have a disjunction_search method")
        def ns_disj_grep():
            lines = ["hello", "hollow", "handy"]

            g = newssearch.Grep(lines)

            # uses boolean OR on search terms
            result = g.disjunction_search(["he", "ha"])
            it.assertEqual([(0, "hello"), (2, "handy")], result)

        @it.should("have a conjunction_search method")
        def ns_conj_grep():
            lines = ["hello", "hollow", "belly"]

            g = newssearch.Grep(lines)

            # uses boolean AND on search terms
            result = g.conjunction_search(["ll", "o"])
            it.assertEqual([(0, "hello"), (1, "hollow")], result)


it.createTests(globals())
