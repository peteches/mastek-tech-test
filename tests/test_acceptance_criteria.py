"""Nose tests"""

import os
import tempfile
from unittest import mock

import requests
from nose2.tools import such

import newssearch

with such.A("newssearch package") as it:

    with it.having("Some acceptance criteria"):

        @it.should("find the correct lines for each search term")
        def acceptance_criteria():

            acceptance_criteria = [
                {
                    "terms": ["Care", "Quality", "Commission"],
                    "operator": "OR",
                    "result_indexes": [0, 1, 2, 3, 4, 5, 6],
                },
                {
                    "terms": ["September", "2004"],
                    "operator": "OR",
                    "result_indexes": [9],
                },
                {
                    "terms": ["general", "population", "generally"],
                    "operator": "OR",
                    "result_indexes": [6, 8],
                },
                {
                    "terms": ["Care", "Quality", "Commission", "admission"],
                    "operator": "AND",
                    "result_indexes": [1],
                },
                {
                    "terms": ["general", "population", "Alzheimer"],
                    "operator": "AND",
                    "result_indexes": [6],
                },
            ]

            for search in acceptance_criteria:
                args = [
                    "--url",
                    "https://gist.githubusercontent.com/edhiley/fdf7793d3d2c9e838c11/raw/ef09a74e925e051c6a756ec54b6adba1787d937b/hscic-news",
                    "--boolean_operation",
                    search["operator"],
                ] + search["terms"]

                result_indexes = [x[0] for x in newssearch.main(args)]

                it.assertEqual(result_indexes, search["result_indexes"])


it.createTests(globals())
