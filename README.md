Newssearch
==========

use pipenv to initialise the environment:

`pipenv run python newssearch.py`

alternatively use the requirements file to manualy create a suitable venv.

Usage:

```
usage: newssearch.py [-h] [-b {OR,AND}] (-f FILE | -u URL)
                     search_terms [search_terms ...]

                     positional arguments:
                       search_terms          terms to search for

                     optional arguments:
                       -h, --help            show this help message and exit
                       -b {OR,AND}, --boolean_operation {OR,AND}
                                             Whether to combine search terms using boolean AND or OR.
                       -f FILE, --file FILE  Path to file containing news to search
                       -u URL, --url URL     Url which will be used to get news to search
```

examples:

`newssearch -b AND --url https://news.com/ baked beans` would GET the contents of https://news.com and print the lines that have both baked and beans in them
