Caching top 5 queries
=====================

inserting data
--------------

```
curl -v http://localhost:8098/riak/cache/CareORQualityORCommission \
	--request PUT \
	--header 'Content-Type:application/json' \
	--data-binary '[0,1,2,3,4,5,6]'
```

```
curl -v http://localhost:8098/riak/cache/SeptemberOR2004 \
	--request PUT \
	--header 'Content-Type:application/json' \
	--data-binary '[9]'
```

```
curl -v http://localhost:8098/riak/cache/generalORpopulationORgenerally \
	--request PUT \
	--header 'Content-Type:application/json' \
	--data-binary '[6,8]'
```

```
curl -v http://localhost:8098/riak/cache/QualityANDCommissionANDadmission \
	--request PUT \
	--header 'Content-Type:application/json' \
	--data-binary '[1]'
```

```
curl -v http://localhost:8098/riak/cache/generalANDpopulationANDAlzheimer \
	--request PUT \
	--header 'Content-Type:application/json' \
	--data-binary '[6]'
```

Retrieving data
---------------

`curl -v http://localhost:8098/riak/cache/CareORQualityORCommission`


Monthly Indexes
===============

inserting data
--------------
```
curl -v http://localhost:8098/riak/types/default/buckets/News/keys/RESULT:0 \
	--request PUT \
	--header 'Content-Type:text/plain' \
	--header 'x-riak-index-month: June' \
	--data 'June 5 , 2013 : The majority of carers say they are extremely'
```
```
curl -v http://localhost:8098/riak/types/default/buckets/News/keys/RESULT:1 \
	--request PUT \
	--header 'Content-Type:text/plain' \
	--data 'July 9 , 2013 : The HSCIC has extended the consultation period'
```
```
curl -v http://localhost:8098/riak/types/default/buckets/News/keys/RESULT:2 \
	--request PUT \
	--header 'Content-Type:text/plain' \
	--data 'June 19 , 2013 : New figures from the Health and Social Care'
```
```
curl -v http://localhost:8098/riak/types/default/buckets/News/keys/RESULT:3 \
	--request PUT \
	--header 'Content-Type:text/plain' \
	--data 'June 13 , 2013 : Almost one in five women who gave birth'
```
```
curl -v http://localhost:8098/riak/types/default/buckets/News/keys/RESULT:4 \
	--request PUT \
	--header 'Content-Type:text/plain' \
	--data 'June 5 , 2013 : The majority of carers say they are extremely'
```
```
curl -v http://localhost:8098/riak/types/default/buckets/News/keys/RESULT:5 \
	--request PUT \
	--header 'Content-Type:text/plain' \
	--data 'April 15 , 2013 Thousands of GP practices around the country'
```
```
curl -v http://localhost:8098/riak/types/default/buckets/News/keys/RESULT:6 \
	--request PUT \
	--header 'Content-Type:text/plain' \
	--data 'February 19 , 2013 : Mortality among mental health service'
```
```
curl -v http://localhost:8098/riak/types/default/buckets/News/keys/RESULT:7 \
	--request PUT \
	--header 'Content-Type:text/plain' \
	--data 'January 23 , 2013 : English A and E departments see the most'
```
```
curl -v http://localhost:8098/riak/types/default/buckets/News/keys/RESULT:8 \
	--request PUT \
	--header 'Content-Type:text/plain' \
	--data 'December 12 , 2012 : The proportion of final year primary'
```
```
curl -v http://localhost:8098/riak/types/default/buckets/News/keys/RESULT:9 \
	--request PUT \
	--header 'Content-Type:text/plain' \
	--data 'September 26 , 2012 : Income before tax for UK contract holding'
```

Retrieving data
---------------
According to the docs: https://docs.riak.com/riak/kv/2.2.3/developing/usage/secondary-indexes/

The below query should work to retrieve the indexes however the riak docker image I used did not
support indexs. So I could not ensure that the above queries do work.

```
$ curl http://localhost:8098/buckets/News/index/month_bin/June
<html><head><title>500 Internal Server Error</title></head><body><h1>Internal Server Error</h1>The server encountered an error while processing this request:<br><pre>{error,{error,{indexes_not_supported,riak_kv_bitcask_backend}}}</pre><P><HR><ADDRESS>mochiweb+webmachine web server</ADDRESS></body></html>%
$
```

Non functional considerations:
==============================

bucket types and n-vals
-----------------------
Bucket types and their configurations play an important part in riak,
or so a quick google tells me.  However I am not familiar enough with
riak to understand or explain why.

riak backend
------------
There are different riak backends but I do not know what they are or how they impact implemention.


replication
-----------
Replication is always an important decision to make with databases.
Use case is of primary concern and for the case of having a caching layer
a Master Slave replication seems reasonable. Provided that the cache implementation can handle
not being able to write to the cache in the event of a master failure, while the slave is promoted.
